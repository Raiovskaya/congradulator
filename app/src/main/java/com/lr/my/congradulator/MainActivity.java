package com.lr.my.congradulator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.lr.my.congradulator.database.actions.MyAction;
import com.lr.my.congradulator.sms.SmsSender;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button send;
    Button contact;
    Button holyday;
    TextView celebration;

    int year;
    int month;
    int day;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        celebration = (TextView)findViewById(R.id.celebration);
        celebration.setText(getHolydayName());

        send = (Button)findViewById(R.id.send);
        send.setOnClickListener(this);

        holyday = (Button)findViewById(R.id.holyday);
        holyday.setOnClickListener(this);

        contact = (Button)findViewById(R.id.contact);
        contact.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.contact:
                Intent intent = new Intent(this,ContactActivity.class);
                startActivity(intent);
                break;
            case R.id.holyday:
                Intent intent2 = new Intent(this,HolydayActivity.class);
                startActivity(intent2);
                break;
            case R.id.send:
                if(celebration.getText().toString().equals("нет праздника")) break;
                SmsSender sender = new SmsSender();
                sender.beginSend(day,month,year);
                break;
            default:
                break;
        }


    }

    private String getHolydayName(){
        Calendar c=Calendar.getInstance();

        year=c.get(c.YEAR);
        month=c.get(c.MONTH)+1;
        day=c.get(c.DAY_OF_MONTH);

        MyAction a = new MyAction();
        return a.getCurrentHolyday(year,month,day);
    }
}
