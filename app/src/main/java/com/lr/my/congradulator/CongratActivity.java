package com.lr.my.congradulator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.lr.my.congradulator.database.CRUD.CongratCRUD;

import java.util.LinkedList;

public class CongratActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, View.OnClickListener{

    Button newItem;

    String hhh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_congrat);

        newItem=(Button)findViewById(R.id.congratNew);
        newItem.setOnClickListener(this);

        Intent intent = getIntent();
        hhh = intent.getStringExtra("holydayName");

        ListView listView = (ListView) findViewById(R.id.congratList);

        CongratCRUD c = new CongratCRUD();
        LinkedList<String> lc=c.getCongratsList(hhh);

        final String [] congratsText = lc.toArray(new String[lc.size()]);


        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, congratsText);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id) {
        TextView textView = (TextView) itemClicked;
        String strText = textView.getText().toString();
        Intent in = new Intent(this,CongratOne.class);
        in.putExtra("text", strText);
        startActivity(in);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.congratNew:
                Intent intent = new Intent(this,EditCongrat.class);
                intent.putExtra("text","newCongrat");
                intent.putExtra("holydayName",hhh);
                startActivity(intent);
                break;
            default:
                break;
        }
    }
}
