package com.lr.my.congradulator.service;

public class MyDate {
    public String buildDate(int day,int month,int year){
        String result=""+day+"-"+month;
        return result;
    }

    public String buildDateforSaving(String date){
        String [] arr = date.split("-");
        String result="";
        int i;
        int flag=0;

        for (String d:arr) {
            flag++;
            i = Integer.parseInt(d);

            if(flag==2){
                result+=i;
            }
            else result+=i+"-";
        }

        return result;
    }
}
