package com.lr.my.congradulator.database.actions;

import com.lr.my.congradulator.database.CRUD.HolydayCRUD;
import com.lr.my.congradulator.database.Congrat;
import com.lr.my.congradulator.database.Holyday;
import com.lr.my.congradulator.database.People;
import com.lr.my.congradulator.service.MyDate;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class MyAction {
    public String getCurrentHolyday(int year,int month,int day){
        String result="";

        MyDate md=new MyDate();
        String date=md.buildDate(day, month, year);


        List<Holyday> holydays = Holyday.find(Holyday.class, "date = ?", date);

        for (Holyday h:holydays) {
            result+=h.name+" ";
        }

        List<People> peoples = People.find(People.class,"birthday = ?",date);

        for (People p:peoples) {
            result+=","+"День рождения "+p.name+p.surname;
        }

        if (result.isEmpty()) return "нет праздника";
        else return result;
    }

    public String getRandomText(String holydayName){

        HolydayCRUD hcrud = new HolydayCRUD();
        Holyday holyday = hcrud.getHolyday(holydayName);
        Random random = new Random();
        Congrat congrat = holyday.getCongrats().get(random.nextInt());
        return congrat.text;
    }

    public List<People> getReceiver(Holyday holyday){
        if(holyday.al) return People.listAll(People.class);
        if(holyday.profession!=null) return People.find(People.class, "profession = ?", holyday.profession);
        if(holyday.malefemale!=null) return People.find(People.class, "gender = ?", holyday.malefemale);

        return new LinkedList<People>();
    }


}
