package com.lr.my.congradulator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.lr.my.congradulator.database.CRUD.HolydayCRUD;
import com.lr.my.congradulator.database.Holyday;
import com.lr.my.congradulator.database.actions.MyAction;
import com.lr.my.congradulator.service.MyDate;

public class EditHolyday extends AppCompatActivity implements View.OnClickListener {

    String operation="edit";

    String holyday;

    EditText editNameHolyday;
    EditText editDate;
    EditText editProfHolyday;
    RadioGroup categoryHolyday;

    String choice;

    Button ok;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_holyday);


        Intent intent = getIntent();
        holyday = intent.getStringExtra("holydayName");

        if (holyday.equals("newHolyday")) {
            operation="create";
        }

        editNameHolyday =(EditText)findViewById(R.id.editNameHolyday);
        editDate=(EditText)findViewById(R.id.editDate);
        editProfHolyday=(EditText)findViewById(R.id.editProfHolyday);
        categoryHolyday=(RadioGroup)findViewById(R.id.categoryHolyday);

        ok=(Button)findViewById(R.id.okHolyday);
        ok.setOnClickListener(this);

        categoryHolyday.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case -1:
                        Toast.makeText(getApplicationContext(), "No choice", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.radioAll:
                        choice="al";
                        break;
                    case R.id.radioFemale:
                        choice="female";
                        break;
                    case R.id.radioMale:
                        choice="male";
                        break;
                    case R.id.radioProf:
                        choice="prof";
                        break;
                    default: break;
                }

            }
        });

        HolydayCRUD hcrud = new HolydayCRUD();


        if (operation.equals("edit")) {
            Holyday h = hcrud.getHolyday(holyday);
            editNameHolyday.setText(h.name);
            editDate.setText(h.date);
            if(!(h.profession==null)) {
                RadioButton rb = (RadioButton)findViewById(R.id.radioProf);
                rb.setChecked(true);
                editProfHolyday.setText(h.profession);
            }
            if(h.al) {
                RadioButton rb = (RadioButton)findViewById(R.id.radioAll);
                rb.setChecked(true);
            }
            if("male".equals(h.malefemale)){
                RadioButton rb = (RadioButton)findViewById(R.id.radioMale);
                rb.setChecked(true);
            }
            if("female".equals(h.malefemale)){
                RadioButton rb = (RadioButton)findViewById(R.id.radioFemale);
                rb.setChecked(true);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.okHolyday:
                String strEditNameHolyday=editNameHolyday.getText().toString();
                MyDate md = new MyDate();
                String strEditDate=md.buildDateforSaving(editDate.getText().toString());
                HolydayCRUD hcrud =new HolydayCRUD();
                Boolean all = false;
                String maleFemale=null;
                String profession=null;

                if (operation.equals("create")){
                    switch (choice){
                        case "al":
                            all = true;
                            break;
                        case "male":
                            maleFemale=choice;
                            break;
                        case "female":
                            maleFemale=choice;
                            break;
                        case "prof":
                            profession= editProfHolyday.getText().toString();
                            break;
                        default:
                            break;
                    }
                    hcrud.createHolyday(strEditNameHolyday, strEditDate, all, profession, maleFemale, "");
                }
                else{
                        switch (choice) {
                            case "al":
                                all = true;
                                break;
                            case "male":
                                maleFemale = choice;
                                break;
                            case "female":
                                maleFemale = choice;
                                break;
                            case "prof":
                                profession = editProfHolyday.getText().toString();
                                break;
                            default:
                                break;
                        }
                        hcrud.updateHolyday(holyday, strEditNameHolyday, strEditDate, all, profession, maleFemale, "");


                }
                Intent in = new Intent(this,MainActivity.class);
                startActivity(in);
                break;
            default:
                break;
        }

    }
}
