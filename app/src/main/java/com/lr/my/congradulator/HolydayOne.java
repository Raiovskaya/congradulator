package com.lr.my.congradulator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.lr.my.congradulator.database.CRUD.HolydayCRUD;
import com.lr.my.congradulator.database.Holyday;

public class HolydayOne extends AppCompatActivity implements View.OnClickListener{

    Button edit;
    Button del;
    Button congrats;

    String holyday;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_holyday_one);

        edit =(Button)findViewById(R.id.editHolyday);
        edit.setOnClickListener(this);

        del =(Button)findViewById(R.id.delHolyday);
        del.setOnClickListener(this);

        congrats =(Button)findViewById(R.id.congratList);
        congrats.setOnClickListener(this);

        Intent intent = getIntent();
        holyday = intent.getStringExtra("holydayName");

        TextView name= (TextView)findViewById(R.id.name);
        TextView date= (TextView)findViewById(R.id.date);
        TextView category= (TextView)findViewById(R.id.category);

        HolydayCRUD hcrud = new HolydayCRUD();
        Holyday holydayEntity=hcrud.getHolyday(holyday);

        name.setText(holydayEntity.name);
        date.setText(holydayEntity.date);
        if(holydayEntity.al) category.setText("Все");
        if(holydayEntity.profession!=null) category.setText(holydayEntity.profession);
        if(holydayEntity.malefemale!=null) category.setText(holydayEntity.malefemale);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.editHolyday:
                Intent in = new Intent(this,EditHolyday.class);
                in.putExtra("holydayName",holyday);
                startActivity(in);
                break;
            case R.id.delHolyday:
                HolydayCRUD hcrud=new HolydayCRUD();
                hcrud.deleteHolyday(holyday);

                Intent in1 = new Intent(this,MainActivity.class);
                startActivity(in1);
                break;
            case R.id.congratList:
                Intent in2 = new Intent(this,CongratActivity.class);
                in2.putExtra("holydayName",holyday);
                startActivity(in2);
                break;
            default:
                break;
        }
    }
}
