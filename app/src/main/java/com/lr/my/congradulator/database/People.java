package com.lr.my.congradulator.database;

import com.orm.SugarRecord;

public class People extends SugarRecord{
    public String name;
    public String surname;
    public String patronymic;
    public String birthday;
    public String profession;
    public String phone;
    public String gender;

    public People() {
    }

    public People(String name, String surname, String patronymic, String birthday, String profession, String phone, String gender) {
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.birthday = birthday;
        this.profession = profession;
        this.phone = phone;
        this.gender = gender;
    }

}
