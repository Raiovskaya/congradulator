package com.lr.my.congradulator.database.CRUD;

import com.lr.my.congradulator.database.Holyday;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class HolydayCRUD {

    public void createHolyday(String name, String date, Boolean all, String profession, String malefemale,String birthday){
        Holyday holyday = new Holyday(name,date,all,profession,malefemale,birthday);
        holyday.save();
    }

    public Holyday getHolyday(String name){
        List<Holyday> holydays =Holyday.find(Holyday.class, "name = ?", name);
        return holydays.get(0);
    }

    public LinkedList<String> getHolydaysList(){
        List<Holyday> holydays =Holyday.listAll(Holyday.class);
        Iterator<Holyday> iter = holydays.iterator();

        LinkedList<String> result = new LinkedList<>();

        while(iter.hasNext()){
            result.add(iter.next().name);
        }

        return result;
    }

    public void updateHolyday(String oldName,String name, String date, Boolean all, String profession, String malefemale,String birthday){
        List<Holyday> holydays =Holyday.find(Holyday.class, "name = ?", oldName);
        Holyday holyday = holydays.get(0);

        holyday.al =all;
        holyday.date=date;
        holyday.name=name;
        holyday.malefemale=malefemale;
        holyday.profession=profession;
        holyday.birthday=birthday;

        holyday.save();
    }

    public void deleteHolyday(String name){
        List<Holyday> holydays =Holyday.find(Holyday.class, "name = ?", name);
        Holyday holyday = holydays.get(0);

        holyday.delete();
    }
}
