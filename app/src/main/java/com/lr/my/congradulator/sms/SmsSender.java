package com.lr.my.congradulator.sms;

import android.telephony.SmsManager;

import com.lr.my.congradulator.database.Holyday;
import com.lr.my.congradulator.database.People;
import com.lr.my.congradulator.database.actions.MyAction;
import com.lr.my.congradulator.service.MyDate;

import java.util.List;

public class SmsSender {
    public void beginSend(int day,int month,int year){
        MyDate md=new MyDate();
        String date=md.buildDate(day, month, year);
        List<Holyday> holydays = Holyday.find(Holyday.class, "date = ?", date);
        List<People> peoples = People.find(People.class,"birthday = ?",date);

        MyAction ma = new MyAction();

        for (People p:peoples) {
            send(p.phone, ma.getRandomText("Birthday"));
        }

        List<People> categoryPeoples;

        for (Holyday h:holydays){
            categoryPeoples=ma.getReceiver(h);
            for(People p:categoryPeoples){
                send(p.phone, ma.getRandomText(h.name));
            }
        }

    }

    private void send(String number,String text){
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(number, null, text, null, null);
    }

}
