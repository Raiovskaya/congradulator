package com.lr.my.congradulator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.lr.my.congradulator.database.CRUD.PeopleCRUD;
import com.lr.my.congradulator.database.People;
import com.lr.my.congradulator.service.MyDate;

public class EditContact extends AppCompatActivity  implements View.OnClickListener{
    String oldName;
    String oldSurname;
    String oldPatronymic;

    String people;

    Button ok;

    TextView name;
    TextView surname;
    TextView patronymic;
    TextView birthday;
    TextView profession;
    TextView phone;
    TextView gender;

    String operation;

    PeopleCRUD pcrud;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_contact);

        pcrud = new PeopleCRUD();

        name = (TextView)findViewById(R.id.editName);
        surname = (TextView)findViewById(R.id.editSurname);
        patronymic = (TextView)findViewById(R.id.editPatronymic);
        birthday = (TextView)findViewById(R.id.editBirth);
        profession = (TextView)findViewById(R.id.editProfession);
        phone = (TextView)findViewById(R.id.editPhone);
        gender = (TextView)findViewById(R.id.editGen);

        Intent intent = getIntent();
        people = intent.getStringExtra("peopleName");

        if(!people.equals("newPeople")){
            operation="edit";
            setPeople(people);
            People entityPeople = pcrud.getPeople(oldName,oldSurname,oldPatronymic);

            name.setText(entityPeople.name);
            surname.setText(entityPeople.surname);
            patronymic.setText(entityPeople.patronymic);
            birthday.setText(entityPeople.birthday);
            profession.setText(entityPeople.profession);
            phone.setText(entityPeople.phone.toString());
            gender.setText(entityPeople.gender);
        }
        else{
            operation="new";
        }

        ok = (Button)findViewById(R.id.contactOk);
        ok.setOnClickListener(this);


    }

    private void setPeople(String p){
        String [] people = p.split(" ");

        oldName=people[0];
        oldSurname=people[1];
        oldPatronymic=people[2];
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.contactOk:
                if(operation.equals("new")){
                    String cname=name.getText().toString();
                    String csurname=surname.getText().toString();
                    String cpatr=patronymic.getText().toString();
                    MyDate md = new MyDate();
                    String cbirth=md.buildDateforSaving(birthday.getText().toString());
                    String cprof=profession.getText().toString();
                    String cphone=phone.getText().toString();
                    String cgen=gender.getText().toString();
                    pcrud.createPeople(cname,csurname,cpatr,cbirth,cprof,cphone,cgen);
                }
                else{
                    pcrud.updatePeople(oldName,oldSurname,oldPatronymic,name.getText().toString(),surname.getText().toString(),patronymic.getText().toString(),birthday.getText().toString(),profession.getText().toString(),phone.getText().toString(),gender.getText().toString());
                }
                Intent in = new Intent(this,MainActivity.class);
                startActivity(in);

            default:
                break;
        }
    }
}
