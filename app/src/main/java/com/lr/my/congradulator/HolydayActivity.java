package com.lr.my.congradulator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.lr.my.congradulator.database.CRUD.HolydayCRUD;

import java.util.LinkedList;

public class HolydayActivity extends AppCompatActivity implements AdapterView.OnItemClickListener,View.OnClickListener {

    Button create;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_holyday);

        create =(Button)findViewById(R.id.createHolyday);
        create.setOnClickListener(this);

        ListView listView = (ListView) findViewById(R.id.listHolydays);

        HolydayCRUD h = new HolydayCRUD();
        LinkedList<String> lh=h.getHolydaysList();

        final String [] holydaysNames = lh.toArray(new String[lh.size()]);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, holydaysNames);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.createHolyday:
                Intent in = new Intent(this,EditHolyday.class);
                in.putExtra("holydayName","newHolyday");
                startActivity(in);
            default:
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        TextView textView = (TextView) view;
        String strText = textView.getText().toString();
        Intent in = new Intent(this,HolydayOne.class);
        in.putExtra("holydayName",strText);
        startActivity(in);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
