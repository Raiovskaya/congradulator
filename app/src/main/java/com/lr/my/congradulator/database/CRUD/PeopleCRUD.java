package com.lr.my.congradulator.database.CRUD;

import com.lr.my.congradulator.database.People;

import java.util.*;

public class PeopleCRUD {

    public void createPeople(String name, String surname, String patronymic, String birthday, String profession, String phone, String gender){
        People people=new People(name,surname,patronymic,birthday,profession,phone,gender);
        people.save();
    }

    public People getPeople(String name, String surname, String patronymic){
        List<People> peoples =People.find(People.class, "name = ? and surname = ? and patronymic = ?", name, surname, patronymic);
        return peoples.get(0);
    }

    public LinkedList<String> getPeoplesList(){
        List<People> peoples =People.listAll(People.class);
        Iterator<People> iter = peoples.iterator();

        LinkedList<String> result = new LinkedList<>();
        People people;


        while(iter.hasNext()){
            people=iter.next();
            result.add(people.name+" "+people.surname+" "+people.patronymic);
        }

        return result;
    }

    public void updatePeople(String oldName, String oldSurname, String oldPatronymic,String name, String surname, String patronymic, String birthday, String profession, String phone, String gender){
        List<People> peoples =People.find(People.class, "name = ? and surname = ? and patronymic = ?", oldName, oldSurname, oldPatronymic);
        People people= peoples.get(0);

        people.name=name;
        people.surname=surname;
        people.patronymic=patronymic;
        people.birthday=birthday;
        people.profession=profession;
        people.phone=phone;
        people.gender=gender;

        people.save();
    }

    public void deletePeople(String name, String surname, String patronymic){
        List<People> peoples =People.find(People.class, "name = ? and surname = ? and patronymic = ?", name, surname, patronymic);
        People people= peoples.get(0);

        people.delete();
    }
}
