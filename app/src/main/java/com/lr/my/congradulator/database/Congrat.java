package com.lr.my.congradulator.database;

import com.orm.SugarRecord;

public class Congrat extends SugarRecord {
    public String text;

    public Holyday holyday;

    public Congrat() {
    }

    public Congrat(String text, Holyday holyday) {

        this.text = text;
        this.holyday = holyday;
    }
}
