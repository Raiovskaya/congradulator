package com.lr.my.congradulator.database;

import com.orm.SugarRecord;

import java.util.List;

public class Holyday extends SugarRecord{
    public String name;
    public String date;
    public Boolean al;
    public String profession;
    public String malefemale;
    public String birthday;


    public Holyday() {
    }

    public Holyday(String name, String date, Boolean al, String profession, String malefemale, String birthday) {

        this.name = name;
        this.date = date;
        this.al = al;
        this.profession = profession;
        this.malefemale = malefemale;
        this.birthday = birthday;
    }

    public List<Congrat> getCongrats() {
        return Congrat.find(Congrat.class, "holyday = ?", new String[]{getId().toString()});
    }


}
