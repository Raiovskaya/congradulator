package com.lr.my.congradulator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.lr.my.congradulator.database.CRUD.CongratCRUD;

public class CongratOne extends AppCompatActivity implements View.OnClickListener{

    TextView holyday;
    TextView text;
    Button edit;
    Button del;
    String str;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_congrat_one);

        text=(TextView)findViewById(R.id.congratName);
        holyday=(TextView)findViewById(R.id.congratText);
        edit=(Button)findViewById(R.id.congratRed);
        edit.setOnClickListener(this);
        del=(Button)findViewById(R.id.congratDel);
        del.setOnClickListener(this);

        Intent intent = getIntent();

        str=intent.getStringExtra("text");

        holyday.setText(str);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.congratDel:
                CongratCRUD ccrud =new CongratCRUD();
                ccrud.deleteCongrat(str);

                Intent in1 = new Intent(this,MainActivity.class);
                startActivity(in1);
                break;
            case R.id.congratRed:
                Intent intent = new Intent(this,EditCongrat.class);
                intent.putExtra("text", str);
                startActivity(intent);
                break;
            default:
                break;
        }
    }
}
