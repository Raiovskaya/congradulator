package com.lr.my.congradulator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.lr.my.congradulator.database.CRUD.CongratCRUD;
import com.lr.my.congradulator.database.CRUD.HolydayCRUD;
import com.lr.my.congradulator.database.Congrat;
import com.lr.my.congradulator.database.Holyday;

public class EditCongrat extends AppCompatActivity implements View.OnClickListener {

    EditText editText;
    Button ok;

    String operation="edit";

    String holyday;
    String text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_congrat);

        Intent intent = getIntent();
        text = intent.getStringExtra("text");

        if (text.equals("newCongrat")) {
            operation="create";
            holyday=intent.getStringExtra("holydayName");
        }

        editText=(EditText)findViewById(R.id.editTextCongr);

        if (operation.equals("edit")) editText.setText(text);

        ok=(Button)findViewById(R.id.congrOk);
        ok.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.congrOk:
                String tx=editText.getText().toString();
                CongratCRUD ccrud = new CongratCRUD();
                if (operation.equals("create")){
                    HolydayCRUD hcrud =new HolydayCRUD();
                    ccrud.createCongrat(hcrud.getHolyday(holyday),tx);
                }
                else{
                    ccrud.updateCongrat(text,tx);
                }
                Intent in = new Intent(this,MainActivity.class);
                startActivity(in);
                break;
            default:
                break;
        }
    }
}
