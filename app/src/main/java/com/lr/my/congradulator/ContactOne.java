package com.lr.my.congradulator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.lr.my.congradulator.database.CRUD.PeopleCRUD;
import com.lr.my.congradulator.database.People;

public class ContactOne extends AppCompatActivity implements View.OnClickListener {

    Button edit;
    Button del;

    String name;
    String surname;
    String patronymic;

    String people;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_one);

        edit = (Button)findViewById(R.id.redCont);
        edit.setOnClickListener(this);

        del = (Button)findViewById(R.id.delCont);
        del.setOnClickListener(this);

        Intent intent = getIntent();
        people = intent.getStringExtra("peopleName");

        setPeople(people);

        PeopleCRUD pcrud = new PeopleCRUD();
        People entityPeople = pcrud.getPeople(name, surname, patronymic);

        TextView name = (TextView)findViewById(R.id.name);
        name.setText(entityPeople.name);

        TextView surname = (TextView)findViewById(R.id.surname);
        surname.setText(entityPeople.surname);

        TextView patronymic = (TextView)findViewById(R.id.patr);
        patronymic.setText(entityPeople.patronymic);

        TextView birthday = (TextView)findViewById(R.id.birth);
        birthday.setText(entityPeople.birthday);

        TextView profession = (TextView)findViewById(R.id.profession);
        profession.setText(entityPeople.profession);

        TextView phone = (TextView)findViewById(R.id.phone);
        phone.setText(entityPeople.phone);

        TextView gender = (TextView)findViewById(R.id.gender);
        gender.setText(entityPeople.gender);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.delCont:
                PeopleCRUD pcrud = new PeopleCRUD();
                pcrud.deletePeople(name, surname, patronymic);
                Intent in = new Intent(this,MainActivity.class);
                startActivity(in);
                break;
            case R.id.redCont:
                Intent in1 = new Intent(this,EditContact.class);
                in1.putExtra("peopleName",people);
                startActivity(in1);
                break;
            default:
                break;
        }
    }

    private void setPeople(String p){
        String [] people = p.split(" ");

        name=people[0];
        surname=people[1];
        patronymic=people[2];
    }
}
