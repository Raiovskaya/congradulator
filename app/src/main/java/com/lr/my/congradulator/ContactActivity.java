package com.lr.my.congradulator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.lr.my.congradulator.database.CRUD.PeopleCRUD;

import java.util.LinkedList;

public class ContactActivity extends AppCompatActivity implements AdapterView.OnItemClickListener,View.OnClickListener {

    Button create;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        create =(Button)findViewById(R.id.contactNew);
        create.setOnClickListener(this);

        ListView listView = (ListView) findViewById(R.id.contactList);

        PeopleCRUD p = new PeopleCRUD();
        LinkedList<String> lp=p.getPeoplesList();

        final String [] peopleNames = lp.toArray(new String[lp.size()]);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, peopleNames);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id) {
        TextView textView = (TextView) itemClicked;
        String strText = textView.getText().toString();
        Intent in = new Intent(this,ContactOne.class);
        in.putExtra("peopleName",strText);
        startActivity(in);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.contactNew:
                Intent in = new Intent(this,EditContact.class);
                in.putExtra("peopleName","newPeople");
                startActivity(in);
            default:
                break;
        }
    }
}
