package com.lr.my.congradulator.database.CRUD;

import com.lr.my.congradulator.database.Congrat;
import com.lr.my.congradulator.database.Holyday;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class CongratCRUD {

    public void createCongrat(Holyday holyday, String text){
        Congrat congrat = new Congrat(text,holyday);
        congrat.save();
    }

    public Congrat getCongrat(String text){
        List<Congrat> congrats=Congrat.find(Congrat.class, "text = ?", text);
        return congrats.get(0);
    }

    public LinkedList<String> getCongratsList(String holyday){
        List<Holyday> holydays =Holyday.find(Holyday.class, "name = ?", holyday);
        List<Congrat> congrats=new LinkedList<>();

        Iterator<Holyday> iter = holydays.iterator();

        while(iter.hasNext()){
            congrats.addAll(iter.next().getCongrats());
        }

        LinkedList<String> result = new LinkedList<>();
        Iterator<Congrat> iter2 = congrats.iterator();

        while(iter2.hasNext()){
            result.add(iter2.next().text);
        }

        return result;
    }

    public void updateCongrat(String oldText, String text){
        List<Congrat> congrats=Congrat.find(Congrat.class, "text = ?", oldText);
        Congrat congrat =congrats.get(0);
        congrat.text = text;
        congrat.save();
    }

    public void deleteCongrat(String text){
        List<Congrat> congrats=Congrat.find(Congrat.class, "text = ?", text);
        Congrat congrat =congrats.get(0);
        congrat.delete();
    }
}
